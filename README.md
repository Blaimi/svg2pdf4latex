# svg2pdf4latex
This project converts layered svg-images with special layer-names into multiple pdf-files.

The targeted use-case for this is to have an easy way to use images inside of [beamer](https://latex-beamer.com/)
presentations when overlay-style transitions are necessary.

## usage

### parameters / input and output
The script can handle any number of parameters where the parameters have to be directories,
each containing a subdirectory `layered` where all the source svgs are saved which should be interpreted.
If no parameter is given, the working directory is used (`.`).
The script creates a new svg-file with the original name, suffixed with `_layer_#` where `#` is the number of the layer.
See the [`test` directory](https://gitlab.com/svg2pdf4latex/svg2pdf4latex/-/tree/main/test) for examples.

### mtime / skip
The script checks the last modified timestamps of the files and skips files where the sourcefile is older than the
targetfile.

### plain
```shell
# install the requirements
apt-get install -y inkscape xmlstarlet
# or
dnf install -y inkscape xmlstarlet

# download the script
git clone https://gitlab.com/svg2pdf4latex/svg2pdf4latex
cd svg2pdf4latex
# or
curl https://gitlab.com/svg2pdf4latex/svg2pdf4latex/-/raw/main/svg2pdf4latex.sh
chmod +x svg2pdf4latex.sh

# execute it
./svg2pdf4latex.sh path/to/your/project
```

### container
you need to mount the path to your project into the container.
To prevent problems with file-permissions, you should execute it with the correct user.
The default `cmd`, i.e. the parameter, is `/app`.
```shell
cd path/to/your/project
docker run --rm -it --user="$(id -u):$(id -g)" -v $PWD:/app svg2pdf4latex/svg2pdf4latex
# or
docker run --rm -it --user="$(id -u):$(id -g)" -v path/to/your/project:/app:Z svg2pdf4latex/svg2pdf4latex:fedora subdirectory
```

## behind the scenes
This project uses a bash script, inkscape, xmlstarlet and other tools to inspect, modify and convert the svg into
multiple pdf files.

### container
There are containers based on ubuntu and fedora.
Rebuilds are daily scheduled and uploaded if necessary, i.e. the base-image or the packages got updates.

#### tags
The tags `ubuntu` and `fedora` always contain the latest skript-version and can break backwards-compatibility.
They are currently based on Ubuntu LTS 22.04 and Fedora 37.
`ubuntu` is the more stable image while `fedora` provides a newer version of inkscape.
If you are unsure, use `ubuntu`, that's why `latest` relies on it.

The images will display several inkscape errors which can be ignored
(dbus-errors from the old implementation on `ubuntu`
and [#7393](https://gitlab.com/inkscape/inbox/-/issues/7393) on `fedora`).

Tags with a single integer as version maintain backwardscompatibility and receive project as well as upstream updates.
You probably want to rely on one of these tags if you integrate it in some automation.

Tags with [semantic versions](https://semver.org/) are never changed and receive no updates.

Tags containing `sched` and a timestamp are copies of scheduled builds and are cleaned up after some time.

#### registries
The container-images can be found at [gitlab.com](https://gitlab.com/svg2pdf4latex/svg2pdf4latex/container_registry/3801191)
as well as on [docker.io](https://hub.docker.com/r/svg2pdf4latex/svg2pdf4latex) and [quay.io](https://quay.io/repository/svg2pdf4latex/svg2pdf4latex)

## fonts
Sadly, inkscape cannot export to pdf without embedding fonts.
Unknown fonts will be replaced when exporting.
This requires all used fonts to be available when executing the script.

### container
The container-images include all fonts from the latest image of [texlive/texlive](https://hub.docker.com/r/texlive/texlive).
If you need your own fonts, you can mount or link them into `/usr/share/local/fonts` to make them available.
You can list all available fonts with the command `docker run --rm -it texlive/texlive luaotfload-tool --list="*"`

### why not using inkscape’s tex-export?
Inkscape can export to pdf by skipping the text in the pdf and providing an additional `.tex`-file for the text.
These files only work if you are using same font and size as the document does.
You can also not scale the image without any workarounds.

### scribus instead of inkscape?
Scribus can export to pdf without embedding fonts, i.e. the font must be available on the system where the resulting pdf
is viewed.
In the case of embedding the pdf as image in a LaTeX-document, the fonts can be embedded from there.
Scribus does not convert the svg as it is to pdf, but converts it first into a scribus-document.
This convertion seems not to be perfect as you can see when opening [`test2/fonts.svg`](https://gitlab.com/svg2pdf4latex/svg2pdf4latex/-/tree/main/test/test2/fonts.svg)

## source code and issue-tracker
This project is hosted on [gitlab.com/svg2pdf4latex/svg2pdf4latex](https://gitlab.com/svg2pdf4latex/svg2pdf4latex) with an enabled
[issue tracker](https://gitlab.com/svg2pdf4latex/svg2pdf4latex/-/issues)

### contributing
To contribute, please open a merge-request on [gitlab](https://gitlab.com/svg2pdf4latex/svg2pdf4latex/-/merge_requests).

## TODO
* document usage in ci/cd and tools (gitlab-ci, docker, plain, Texify Idea)
* test and document with podman
* convert bash to python to enable easier logging with debug-levels, unit-tests and parameter-handling
* allow settings like the name of the `layered`-directory.
* man-page
* themeing?
* deb and/or rpm-packaging
