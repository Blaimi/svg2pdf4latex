#!/usr/bin/env bash

# current state:
#   <1-5>:  layer 1 to 5
#   <-3>:   layer 1 to 3
#   <2->:   layer 2 to the highest of all mentioned layers
#   <7>:    layer 7
#   <1,5>:  layer 1 and 5
#   <3,7->: layer 3 and 7 to the end

function delete_layer_in {
  node_id=$1
  layer_svgname=$2
  xmlstarlet ed -P --inplace -d '//svg:svg/svg:g[@id="'"${node_id}"'"]' "$layer_svgname"
}

function prepare_layers {
  svgname="$1"
  label_regex='^&lt;([0-9]*(-[0-9]*)?(,[0-9]*(-[0-9]*)?)*)&gt;.*$'

  # collect the layers
  mentioned=()
  while read -r label; do
    echo "$label" | grep -E "${label_regex}" > /dev/null 2>&1 || continue
    visible=$(echo "$label" | sed -E "s~${label_regex}~\1~")
    IFS="," read -ra parts <<< "$visible"
    for part in "${parts[@]}"; do
      IFS="-" read -ra fromto <<< "$part"
      mentioned+=( "${fromto[@]}" )
    done <<< "$visible"
  done < <(xmlstarlet sel -t -v '//svg:svg/svg:g/@inkscape:label' -n "$svgname")
  # https://stackoverflow.com/a/7442583/568291
  readarray -t mentioned_sorted < <(printf '%s\0' "${mentioned[@]}" | sort -z | xargs -0n1)
  highest_mentioned="${mentioned_sorted[-1]}"

  # copy the source to the target svgs and delete layers which should not be visible
  for ((layer_number=1; layer_number <= highest_mentioned; layer_number++)); do
    layer_svgname="$(dirname "$(dirname "$svgname")")/$(basename "$svgname" .svg)_layer_${layer_number}.svg"
    [ "$svgname" -ot "$layer_svgname" ] && continue
    cp "$svgname" "$layer_svgname"
    while read -r node_id; do
      label=$(xmlstarlet sel -t -v '/svg:svg/svg:g[@id="'"${node_id}"'"]/@inkscape:label' "$svgname")
      echo "$label" | grep -E "${label_regex}" > /dev/null 2>&1 || continue
      visible=$(echo "$label" | sed -E "s~${label_regex}~\1~")
      found="false"
      IFS="," read -ra parts <<< "$visible"
      for part in "${parts[@]}"; do
        [[ "$part" == *"-"* ]] || part="${part}-${part}"
        IFS="-" read -r from to <<< "$part"
        [ "$from" = "" ] && from="1"
        [ "$to" = "" ] && to="${highest_mentioned}"
        [ "$layer_number" -ge "$from" ] && [ "$layer_number" -le "$to" ] && found="true"
      done <<< "$visible"
      [ "$found" == "false" ] && { delete_layer_in "${node_id}" "${layer_svgname}";}
    done < <(xmlstarlet sel -t -v '//svg:svg/svg:g/@id' -n "$svgname")
  done
}

args=("$@")
if [ $# -eq 0 ]; then
  args=(".")
fi

errored="false"
for srcdir in "${args[@]}"; do
  # check if there is at least one svg-files in $srcdir/layered
  for f in "$srcdir"/layered/*.svg; do [ -f "$f" ] || { echo "no files found in $srcdir/layered/*.svg"; errored="true"; }; done
done

[ "$errored" == "true" ] && exit 1

for srcdir in "${args[@]}"; do
  for svgname in "$srcdir"/layered/*.svg; do
    prepare_layers $svgname
  done

  for svgname in "$srcdir"/*.svg; do
    pdfname=$(dirname "$svgname")/$(basename "$svgname" .svg).pdf
    [ "$svgname" -ot "$pdfname" ] && continue
    inkscape "$svgname" -o "$pdfname" --export-ignore-filters --export-ps-level=3
  done
done
