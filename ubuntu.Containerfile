# the dry-run build stage is to prevent the creation of new images when no updates are available
# the resulting image can use layer-caching

FROM ubuntu:22.04 AS dry-run
# invalidate layer cache
ADD "https://www.random.org/integers/?num=1&min=1&max=1000000000&col=1&base=10&format=plain&rnd=new" "/tmp/rand"
RUN apt-get update \
    && apt-get upgrade --dry-run | tee /tmp/apt-upgrade-dryrun \
    && apt-get --no-install-recommends install \
    inkscape \
    xmlstarlet \
    curl ca-certificates \
    --dry-run | tee /tmp/apt-install-dryrun \
    && rm -rf /var/lib/apt/lists/*

FROM ubuntu:22.04 AS svg2pdf4latex
LABEL org.opencontainers.image.authors="blaimi@blaimi.de"
COPY --from=dry-run /tmp/apt-upgrade-dryrun /tmp/apt-upgrade-dryrun
COPY --from=dry-run /tmp/apt-install-dryrun /tmp/apt-install-dryrun
RUN rm -f /tmp/apt-upgrade-dryrun /tmp/apt-install-dryrun
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get upgrade -y && apt-get --no-install-recommends install -y \
    inkscape \
    xmlstarlet \
    curl ca-certificates \
    && rm -rf /var/lib/apt/lists/*
COPY --from=docker.io/texlive/texlive /usr/local/texlive/*/texmf-dist/fonts/* /usr/local/share/fonts/
COPY svg2pdf4latex.sh /usr/local/bin
ENV HOME=/tmp
WORKDIR /app
ENTRYPOINT [ "svg2pdf4latex.sh" ]
