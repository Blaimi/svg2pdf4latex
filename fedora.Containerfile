# the dry-run build stage is to prevent the creation of new images when no updates are available
# the resulting image can use layer-caching

FROM fedora:37 AS dry-run
# invalidate layer cache
ADD "https://www.random.org/integers/?num=1&min=1&max=1000000000&col=1&base=10&format=plain&rnd=new" "/tmp/rand"
# tail is used to skip the line(s) about metedata-downloads
RUN { dnf upgrade --refresh --assumeno || true; } | tee >(tail -5 > /tmp/dnf-upgrade-dryrun) \
    && { dnf install -y --setopt=install_weak_deps=False \
    inkscape \
    xmlstarlet \
    --assumeno || true; } | tee >(tail -1 > /tmp/dnf-install-dryrun) \
    && dnf clean all

FROM fedora:37 AS svg2pdf4latex
LABEL org.opencontainers.image.authors="blaimi@blaimi.de"
COPY --from=dry-run /tmp/dnf-upgrade-dryrun /tmp/dnf-upgrade-dryrun
COPY --from=dry-run /tmp/dnf-install-dryrun /tmp/dnf-install-dryrun
RUN rm -f /tmp/dnf-upgrade-dryrun /tmp/dnf-install-dryrun
RUN dnf upgrade -y --refresh && dnf install -y --setopt=install_weak_deps=False \
    inkscape \
    xmlstarlet \
    && dnf clean all
COPY --from=docker.io/texlive/texlive /usr/local/texlive/*/texmf-dist/fonts/* /usr/local/share/fonts/
COPY svg2pdf4latex.sh /usr/local/bin
ENV HOME=/tmp
WORKDIR /app
ENTRYPOINT [ "svg2pdf4latex.sh" ]
